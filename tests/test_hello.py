"""Basic test"""
from click.testing import CliRunner

from generic_project.cli import hello


def test_hello_world():
    """Check that click works as intended"""
    runner = CliRunner()
    result = runner.invoke(hello)
    assert result.exit_code == 0
    assert result.output == "Hello World!\n"
