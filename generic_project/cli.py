"""Generic project commands declaration"""
import click


@click.command()
def hello():
    """Simple click command"""
    click.echo("Hello World!")
